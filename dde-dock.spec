%global sname deepin-dock

Name:           dde-dock
Version:        5.5.76
Release:        1
Summary:        Deepin desktop-environment - Dock module
License:        GPLv3
URL:            https://github.com/linuxdeepin/dde-dock
Source0:        %{name}-%{version}.tar.gz
Patch1:         0001-Hidden-state.patch
Patch2:         0001-hide-status-box.patch
Patch3:         0001-Mask-intelligent-hiding.patch

BuildRequires:  cmake
BuildRequires:  gcc-c++
BuildRequires:  libarchive
BuildRequires:  pkgconfig(dbusmenu-qt5)
BuildRequires:  dde-network-core-devel
BuildRequires:  dtkwidget-devel >= 5.1
BuildRequires:  dtkcore-devel >= 5.1
BuildRequires:  dtkcommon-devel
BuildRequires:  pkgconfig(dframeworkdbus) >= 2.0
BuildRequires:  pkgconfig(gsettings-qt)
BuildRequires:  pkgconfig(gtk+-2.0)
BuildRequires:  pkgconfig(Qt5Core)
BuildRequires:  pkgconfig(Qt5Gui)
BuildRequires:  pkgconfig(Qt5DBus)
BuildRequires:  pkgconfig(Qt5X11Extras)
BuildRequires:  pkgconfig(Qt5Svg)
BuildRequires:  pkgconfig(x11)
BuildRequires:  pkgconfig(xtst)
BuildRequires:  pkgconfig(xext)
BuildRequires:  pkgconfig(xcb-composite)
BuildRequires:  pkgconfig(xcb-ewmh)
BuildRequires:  pkgconfig(xcb-icccm)
BuildRequires:  pkgconfig(xcb-image)
BuildRequires:  qt5-linguist
BuildRequires:  gtest-devel
BuildRequires:  gmock-devel
BuildRequires:  qt5-qtbase-private-devel
BuildRequires:  qt5-qttools-devel
BuildRequires:  libXtst-devel
BuildRequires:  libxcb
BuildRequires:  dde-control-center-devel >= 5.5.77
BuildRequires:  dde-control-center >= 5.5.77

Requires:       dbusmenu-qt5
Requires:       dde-network-core
Requires:       dde-qt-dbus-factory
Requires:       xcb-util-wm
Requires:       xcb-util-image
Requires:       libxcb
Requires:       deepin-desktop-schemas
Requires:       dde-daemon
Requires:       libqtxdg-devel
Requires:       onboard

%description
Deepin desktop-environment - Dock module.

%package devel
Summary:        Development package for %{sname}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
Header files and libraries for %{sname}.

%package onboard-plugin
Summary:        deepin desktop-environment - dock plugin
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description onboard-plugin
deepin desktop-environment - dock plugin.

%prep
%setup -q
%patch1 -p1
%patch2 -p1
%patch3 -p1

find plugins/ -mindepth 2 -maxdepth 2 -type f -name CMakeLists.txt | grep -vE 'dcc-dock-plugin/CMakeLists.txt' | xargs -I '{file_name}' sed -i '/TARGETS/s|lib|%{_lib}|' '{file_name}'

sed -i '/TARGETS/s|lib|%{_lib}|' plugins/plugin-guide/plugins-developer-guide.md

sed -i 's|/lib|/%{_lib}|' frame/controller/dockpluginscontroller.cpp \
                          frame/window/mainpanelcontrol.cpp \
                          plugins/tray/system-trays/systemtrayscontroller.cpp


sed -i 's|/lib|/libexec|g' plugins/show-desktop/showdesktopplugin.cpp \
                           frame/window/mainpanelcontrol.cpp \
                           frame/window/components/desktop_widget.cpp

sed -i 's:libdir.*:libdir=%{_libdir}:' dde-dock.pc.in

sed -i 's|/usr/lib/dde-dock/plugins|%{_libdir}/dde-dock/plugins|' plugins/plugin-guide/plugins-developer-guide.md
sed -i 's|local/lib/dde-dock/plugins|local/%{_lib}/dde-dock/plugins|' plugins/plugin-guide/plugins-developer-guide.md

sed -i 's|lrelease|lrelease-qt5|' translate_generation.sh

%build
export PATH=%{_qt5_bindir}:$PATH
%cmake -DCMAKE_INSTALL_PREFIX=%{_prefix} -DARCHITECTURE=%{_arch} -DCMAKE_INSTALL_SYSCONFDIR=/etc .
%make_build

%install
%make_install INSTALL_ROOT=%{buildroot}

%ldconfig_scriptlets

%files
%{_datadir}/%{name}/translations/*.qm
%license LICENSE
%{_bindir}/%{name}
%{_libdir}/%{name}/
%{_datarootdir}/glib-2.0/schemas/com.deepin.dde.dock.module.gschema.xml
%{_datarootdir}/polkit-1/actions/com.deepin.dde.dock.overlay.policy
%config %{_sysconfdir}/%{name}/indicator/keybord_layout.json
%{_datadir}/dsg/
%{_datadir}/dcc-dock-plugin/
%{_prefix}/lib/dde-control-center/modules/
%{_sysconfdir}/%{name}/

%files devel
%doc plugins/plugin-guide
%{_includedir}/%{name}/
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/cmake/DdeDock/DdeDockConfig.cmake

%files onboard-plugin
%{_libdir}/dde-dock/plugins/libonboard.so


%changelog
* Wed Aug 02 2023 leeffo <liweiganga@uniontech.com> - 5.5.76-1
- upgrade to version 5.5.76

* Wed Mar 29 2023 liweiganga <liweiganga@uniontech.com> - 5.4.56.2-1
- update: update to 5.4.56.2

* Mon Jul 18 2022 konglidong <konglidong@uniontech.com> - 5.4.12.7-1
- Update to 5.4.12.7

* Fri Jun 17 2022 loong_C <loong_c@yeah.net> - 5.3.0.31-2
- fix build error

* Thu Jul 08 2021 weidong <weidong@uniontech.com> - 5.3.0.31-1
- Update to 5.3.0.31

* Thu Sep 3 2020 weidong <weidong@uniontech.com> - 5.1.0.13-2
- fix source url in spec

* Thu Jul 30 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.1.0.13-1
- Package init

